terraform {  
  backend "s3" {
      bucket = "danieloikle.terraform.state"
      key = "terraform.tfstate"
      region = "us-east-1"
      shared_credentials_file = "C:\\GitLab-Runner\\.aws\\credentials"
  }
}