UPDATE JUNE 21:
- Added terraform remote state tracking to an amazon S3 bucket with versioning.
- Configured GitLab CI on project to trigger on project changes. Doing so will gather the state from S3, destroy it if it exists and then completely recreate it.

- Temporarily removed ansible playbook invocation, due to the the newly configured GitLab CI Runner being installed on Windows. Will be re-added soon.

TODO:
- Tighten AWS security for GitLab Runner
- Re-implement ansible playbook
- Considering extra branches with CloudFormation and SDK Usage instead of terraform.

Fully automated CICD deployment of Redis
Terraform will provision new infrastructure on AWS, including new IAM accounts. Potentially transition to aws cdk.
Ansible to automate redis prerequisites, build, install and testing.

